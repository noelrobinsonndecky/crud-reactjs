export default function appReducer(state, action) {

    switch (action.type) {
        
            // Ajout d'un employé
        case "ADD_EMPLOYEE":
            return {
                ...state,
                employees: [...state.employees, action.payload],
            };
        
            // Edition d'un employé
        case "EDIT_EMPLOYEE":
        const updatedEmployee = action.payload;
  
        const updatedEmployees = state.employees.map((employee) => {

            if (employee.id === updatedEmployee.id) {
                return updatedEmployee;
            }
                
            return employee;
        });
  
        return {
          ...state,
          employees: updatedEmployees,
        };
  
            // Supression d'un employé
        case "REMOVE_EMPLOYEE":
            return {
                ...state,
                employees: state.employees.filter(
                (employee) => employee.id !== action.payload
                ),
        };
  
        default:
            return state;
    }

};